<?php

namespace Drupal\bug_tracker\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the bug_report entity edit forms.
 *
 * @package Drupal\bug_tracker\Form
 */
class BugReportEditForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\pri_printable_selects\Entity\PrintableItem */
    $form = parent::buildForm($form, $form_state);

    $bugReport = $this->getEntity();

    $form['page_url'] = [
      '#type' => 'processed_text',
      '#title' => $this->t('Related page URL'),
      '#text' => $bugReport->page_url->value,
    ];

    $form['user_agent_data'] = [
      '#type' => 'processed_text',
      '#title' => $this->t('Usert Agent data'),
      '#text' => $bugReport->user_agent_data->value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.bug_report.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

}
