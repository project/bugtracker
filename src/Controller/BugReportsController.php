<?php

namespace Drupal\bug_tracker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\bug_tracker\Entity\BugReportEntity;

/**
 * Class BugReportsController.
 *
 * @package Drupal\bug_tracker\Controller
 */
class BugReportsController extends ControllerBase {

  /**
   * Create bug report callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createBugReport(Request $request) {

    $bugReportParams = $request->request->all();
    $fileData = $request->request->get('screenshot');
    $file_name = str_replace(' ', '-', $request->request->get('title'));
    $file_name = str_replace('/', '_', $file_name);

    $img = str_replace('data:image/png;base64,', '', $fileData);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = file_save_data($data, 'public://bug_reports/' . $file_name . '.png');

    try {
      $bugReport = BugReportEntity::create($bugReportParams);
      $bugReport->screenshot = $file;
      $bugReport->save();
      return new JsonResponse($bugReport, 200);
    }
    catch (EntityStorageException $exception) {
      return new JsonResponse(NULL, 500);
    }
  }

}
