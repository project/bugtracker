<?php

namespace Drupal\bug_tracker\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a 'Report bug' block.
 *
 * @Block(
 *   id = "report_bug",
 *   admin_label = @Translation("Report bug"),
 *   category = @Translation("Custom block"),
 * )
 */
class ReportBugBlock extends BlockBase {

 /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

 /**
   * Constructs a new MyBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

/**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'bug_tracker_report_bug_block',
      '#attached' => [
        'library' => [
          'bug_tracker/html2canvas',
          'bug_tracker/cropper',
          'bug_tracker/bug_tracker',
        ],
      ],
      '#statuses_terms' => $this->getBugsTaxonomyTree('bugs_statuses'),
      '#priorities_terms' => $this->getBugsTaxonomyTree('bugs_priorities'),
    ];
  }

  /**
   * Get taxonomy tree by provided vid value.
   *
   * @param $vid
   *   Value to use to load taxonomy tree.
   *
   * @return array
   *   Taxonomy terms.
   */
  private function getBugsTaxonomyTree($vid) {
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vid);
    $term_data = [];
    foreach ($terms as $term) {
      $term_data[] = [
        "id" => $term->tid,
        "name" => $term->name
      ];
    }
    return $term_data;
  }

}
