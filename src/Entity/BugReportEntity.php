<?php

namespace Drupal\bug_tracker\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the BugReport entity.
 *
 * @ingroup bug_tracker
 *
 * @ContentEntityType(
 *   id = "bug_report",
 *   label = @Translation("Bug report entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\bug_tracker\Entity\Controller\BugReportListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\bug_tracker\Form\BugReportEditForm",
 *       "delete" = "Drupal\bug_tracker\Form\BugReportDeleteForm",
 *     },
 *     "access" = "Drupal\bug_tracker\BugReportAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "bug_reports",
 *   admin_permission = "administer bug_reports entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/bug_reports/{bug_report}",
 *     "edit-form" = "/admin/bug_reports/{bug_report}",
 *     "delete-form" = "/admin/bug_reports/{bug_report}/delete",
 *     "collection" = "/admin/bug_reports/list"
 *   },
 *   field_ui_base_route = "bug_tracker.settings",
 * )
 */
class BugReportEntity extends ContentEntityBase implements ContentEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [];
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * Return title of entity.
   *
   * @return string
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * Return fid of 'screenshot' field.
   *
   * @return integer
   */
  public function getFileId() {
    return $this->get('screenshot')->target_id;
  }

  /**
   * Return fid of 'screenshot' field.
   *
   * @return integer
   */
  public function getFileUri() {
    return $this->get('screenshot')->entity->getFileUri();
  }


  /**
   * {@inheritdoc}
   *
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t(''))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t(''))
      ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setLabel(t('Title'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue(NULL)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t(''))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['screenshot'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Attached screenshot'))
      ->setReadOnly(TRUE)
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'bug_reports/items',
        'alt_field_required' => FALSE,
        'file_extensions' => 'png jpg jpeg',
      ])
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['page_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Page url'))
      ->setDescription(t('Related page url'))
      ->setDisplayOptions('form', [
        'type' => 'processed_text',
        'weight' => -9,
      ]);

    $fields['user_agent_data'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('User Agent Data'))
      ->setDescription(t('Info from browser widnow object'))
      ->setDisplayOptions('form', [
        'type' => 'processed_text',
        'weight' => -8,
      ]);

    $fields['status'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Bug status'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings',
        [
          'target_bundles' => [
            'bugs_statuses' => 'bugs_statuses'
          ]
        ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -6,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '10',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['priority'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Bug priority'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings',
        [
          'target_bundles' => [
            'bugs_priorities' => 'bugs_priorities'
          ]
        ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '10',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
