# SIMPLE PAYPAL FIELD

## INTRODUCTION
This module provides a `Bug` button to report bug with screenshot, description, status, priority, window and browser parameters.

## REQUIREMENTS
Drupal core. No external modules or libraries are required.

## INSTALLATION
`composer require drupal/bugtracker` or any other standard way of
installing a drupal module.

## CONFIGURATION
First, you need to place custom block `Report bug` to needed region.
After that, you will be able to see `Bug` button on selected region.
Bug info will provide `Window height`, `Window width` and `Browser info` automatically. You will need to fill in Fields `Bug status`, `Bug priority` and `Description` to report bug.

## USAGE
The list of reported bugs is available there: `/admin/config/development/bug_tracker`
