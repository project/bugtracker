
(function ($) {
  Drupal.behaviors.openReportBugWindow = {
    attach: function(context) {
      $("#report-bug").once().click(function (event) {
        closeModal();
        report();
      })
    }
  };

  Drupal.behaviors.closeReportBugWindow = {
    attach: function(context) {
      $("#bug-modal-overlay").once().click(function (event) {
        closeModal();
      })
    }
  };

})(jQuery, window, document);

async function report() {
  let screenshotUrl = await makeScreenshot();
  let pastedImage = document.getElementById("bug-report_screenshot");
  pastedImage.src = screenshotUrl; 
  let modalContainer = document.querySelector(".bug-container-modal-layer");
  modalContainer.classList.remove('hide')

  let userAgentValues = await getUserAgentValues();

  let heightInfo = document.getElementById('bug-report__window-height');
  let widthInfo = document.getElementById('bug-report__window-width')
  let userAgentInfo = document.getElementById('bug-report__user-agent-info')
  heightInfo.innerHTML = userAgentValues.height;
  widthInfo.innerHTML = userAgentValues.width;
  userAgentInfo.innerHTML = userAgentValues.appInfo;

  await awaitSendClick();
  await sendRequest(screenshotUrl, userAgentValues); 
  modalContainer.classList.add('hide');
}

async function  makeScreenshot() {
  return new Promise((resolve, reject) => {  
    let node = document.querySelector('body');
    html2canvas(node, {
      width: window.outerWidth,
      height: window.outerHeight,
      y: window.scrollY
    }).then(canvas => {
      let pngUrl = canvas.toDataURL();    
      resolve(pngUrl);
    });
  });
}

async function awaitSendClick(box) {
  let sendButton = document.querySelector(".send-button");
  return new Promise((resolve, reject) => {
    sendButton.addEventListener("click", e => {
      resolve({});
    });
  });
}

async function sendRequest(screenshotUrl, userAgentValues) {

  let formData = new FormData();
  let req = new XMLHttpRequest();
  let params = await this.getParams();
  let userAgentData = `Widnow height: ${userAgentValues.height}, Window width: ${userAgentValues.width}, User Agent: ${userAgentValues.appInfo}`;

  formData.append("screenshot", screenshotUrl);
  formData.append("description", params.description);
  formData.append("user_agent_data", userAgentData);
  formData.append("status", params.status_id);
  formData.append("priority", params.priority_id);
  formData.append("page_url", params.page_url);
  formData.append("title", params.title);
  req.open("POST", '/api/bug_tracker/new_report');
  req.send(formData);
}

async function getParams() {
  return new Promise((resolve, reject) => {  
    let params = {
      page_url: window.location.href,
      title: generateTitle(document.querySelector('.bug-description-field').value),
      description: document.querySelector('.bug-description-field').value,
      status_id: document.querySelector('#bug_status').value,
      priority_id: document.querySelector('#bug_priority').value,
    }
    resolve(params);
  });
}

function closeModal() {
  document.querySelector(".bug-container-modal-layer").classList.add('hide')
  document.querySelector('.bug-description-field').value = null;
}

async function getUserAgentValues() {
  return new Promise((resolve, reject) => {  
    let width = window.outerWidth
    let height = window.outerHeight;
    let appInfo = window.navigator.appVersion;
    resolve({ width, height, appInfo });
  });
}

function generateTitle(string) {
  let title = string.split(/\s+/).slice(0, 8).join(" ");;
  let date = new Date();
  return `${title} - ${date.getFullYear()}/${date.getMonth()}/${date.getDate()}  ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}
